#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "time.h"


#define NUM_THREADS 5
pthread_t producers[2]; 
pthread_t consumers[4];
pthread_mutex_t mutex;
pthread_cond_t notEmpty, notFull;

int MAX = 4;
int buffer = 5;

struct node 
{
	struct node * next;
	int data;
	
	
};

struct node * head;


void put(int d)
{
	struct node * new = malloc(sizeof(struct node));
	new = head;
	while(new->next!=NULL)
	{
		new=new->next;
	}
	new->next=head;
	new->data=d;
	
}

int isFull(struct node * node)
{
	
	int count=0;
	struct node * temp;
	temp=head;
	
	while(temp->next!=NULL)
	{
		
		count++;
		temp=temp->next;
		
	}
	if(count==buffer)
	{
		
		return 1;
	}
	return 0;
}

int isEmpty(struct node * head)
{
	
	int count=0;
	struct node * temp;
	
	temp=head;
	
	while(temp==NULL)
	{
		
		count++;
		temp=temp->next;
		
	}
	if(count==buffer)
	{
		return 0;
	}
	free(temp);
	
	return 1;
}
   
int get()
{
	struct node * temp;
	temp = head;
	while(temp->next!=NULL)
	{
		temp= temp->next;
	}
	int data = temp->data;
	free(temp);
	return data;
}

void * producer(void*arg) {
	int i=0;
	while(1)
	{
		
		pthread_mutex_lock(&mutex); 
		
		while (isFull(head)==1)
		{
			pthread_cond_wait(&notEmpty, &mutex);
		}
		put(i);
		i++;
		pthread_cond_signal(&notEmpty);
		pthread_mutex_unlock(&mutex);
		printf("Producer produced good %d ", i);
		printf("\n");
	}
	
} 
		
void * consumer(void *arg) {
	
	while(1)
	{
	
		pthread_mutex_lock(&mutex); 
		
		while (isEmpty(head)==1)
		{
			pthread_cond_wait(&notFull, &mutex);
		}
		
    	int tmp = get();
		
		pthread_cond_signal(&notFull);
		pthread_mutex_unlock(&mutex);
		printf("%d\n", tmp);
	
	}
	
}

int main()
{
	
	
	head = malloc(sizeof(struct node));
	
	head->next=NULL;
	head->data=NULL;
	
	
	int rc,t, mt;
	for(int i=0; i<2; i++)
	{
		mt =pthread_create(&producers[i], NULL, producer, (void *)i);
	}
	for (int t=0; t<4;t++)
	{
		rc = pthread_create(&consumers[t], NULL, consumer, (void *)t);
		
	}
	
	
	
}