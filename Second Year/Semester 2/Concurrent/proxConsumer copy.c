#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "time.h"

cond_t empty, fill;
mutex_t mutex;

int MAX = 4;

int buffer[MAX];
int fill_ptr =0;
int use_ptr =0;
int count =0;

void put(int value)
{
	 buffer[fill_ptr] = value;
	 fill_ptr = (fill_ptr + 1) % MAX;
	 count++;
}
   
int get() 
{
	int tmp = buffer[use_ptr];
	use_ptr = (use_ptr + 1) % MAX;
	count--;
	return tmp;
}


void *producer(void*arg) {
	int i;
	for(i=0;i<loops; i++)
	{
		Pthread_mutex_lock(&mutex); 
		while (count == MAX)
		{
			 Pthread_cond_wait(&empty, &mutex);
		}
		put(i);
		int tmp = get();
		Pthread_cond_signal(&fill);
		Pthread_mutex_unlock(&mutex);
	}
} 
		
   


void *consumer(void *arg) {
	int i;
	for(i=0;i<loops;i++)
	{ 
		Pthread_mutex_lock(&mutex); 
		while (count == 0)
		{
			Pthread_cond_wait(&fill, &mutex);
		}
    	int tmp = get();
		Pthread_cond_signal(&empty);
		Pthread_mutex_unlock(&mutex);
		printf("%d\n", tmp);
	
	}
}